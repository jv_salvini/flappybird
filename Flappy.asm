;	Org. Comp.

;	Joao Vitor Salvini
;	Felipe Vinicius de Souza
;	Joao Pedro Kosur
;	Sara Dib

jmp main

Letra: var #1		; Contem a letra que foi digitada

posBird: var #1			; Contem a posicao atual da Bird
posAntBird: var #1		; Contem a posicao anterior da Bird

scoredezena : var #1
scoreunidade : var #1
scorecentena : var #1

static scoreunidade, #'0'
static scoredezena, #'0'
static scorecentena, #'0'

posicaodezena : var #1
posicaounidade : var #1
posicaocentena : var #1

static posicaounidade, #77
static posicaodezena, #76
static posicaocentena, #75


;Codigo principal
main:    
	
	call GeraCenario
	
	loadn r0, #160			
	store posBird, r0		; Zera Posicao Atual da Bird
	store posAntBird, r0	; Zera Posicao Anterior da Bird


	
	loadn r0, #0			; Contador para os Mods	= 0
	loadn r2, #0			; Para verificar se (mod(c/10)==0

	Loop:
	
		loadn r1, #10
		mod r1, r0, r1
		cmp r1, r2		; if (mod(c/10)==0
		ceq MoveBird	; Chama rotina de movimentacao da Bird
		call Delay
		
		inc r0 	;
		jmp Loop
		

GeraCenario:
	loadn r0, #pipe ; Caracter que constitui a parede
			
	loadn r1, #0
	loadn r2, #1200

	loop1:	cmp r1,r2
			jeq fimcenario
			loadi r3,r0
			outchar r3,r1
			inc r1
			inc r0
			jmp loop1
			
	fimcenario: rts
;Funcoes
;--------------------------

MoveBird:
	push r0
	push r1
	
	call MoveBird_recalculaPos		; recalcula Posicao da Bird
; So' Apaga e redezenha se (pos != posAnt)
;	If (posBird != posAntBird)	{	
	load r0, posBird
	load r1, posAntBird
	cmp r0, r1
	jeq MoveBird_Skip
	call MoveBird_Apaga
	call MoveBird_Desenha
	
		
  MoveBird_Skip:
	
	pop r1
	pop r0
	rts

;--------------------------------
	
MoveBird_Apaga:		; Apaga a Bird preservando o Cenario!
	push r0
	push r1
	push r2
	push r3
	push r4
	push r5

	load r0, posAntBird	; r0 = posAnt
	
	; --> r2 = Tela1Linha0 + posAnt + posAnt/40  ; tem que somar posAnt/40 no ponteiro pois as linas da string terminam com /0 !!

	loadn r1, #tela0Linha0	; Endereco onde comeca a primeira linha do cenario!!
	add r2, r1, r0	; r2 = Tela1Linha0 + posAnt
	loadn r4, #40
	div r3, r0, r4	; r3 = posAnt/40
	add r2, r2, r3	; r2 = Tela1Linha0 + posAnt + posAnt/40
	
	loadi r5, r2	; r5 = Char (Tela(posAnt))
	
	outchar r5, r0	; Apaga o Obj na tela com o Char correspondente na memoria do cenario
	
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	rts
;----------------------------------	
	
MoveBird_recalculaPos:		; recalcula posicao da Bird em funcao das Teclas pressionadas

	push r0
	push r1
	push r2
	push r3
	
	load r0, posBird
	
	inchar r1				; Le Teclado para controlar a Bird
		
	loadn r2, #'w'
	cmp r1, r2
	jeq MoveBird_RecalculaPos_W
	
	
	call Delay 
	loadn r1, #1159
	cmp r0, r1		; Testa condicoes de Contorno 
	jgr Gameover
	loadn r1, #80
	cmp r0, r1		; Testa condicoes de Contorno 
	jle Gameover
	loadn r1, #41
	add r0, r0, r1	; pos = pos + 41
	jmp MoveBird_RecalculaPos_Fim
		

	
  MoveBird_RecalculaPos_Fim:	; Se nao for nenhuma tecla valida, vai embora
	store posBird, r0
	call Colisao
	pop r3
	pop r2
	pop r1
	pop r0
	rts
	
		
  MoveBird_RecalculaPos_W:	; Move Bird para Cima
	loadn r1, #80
	loadn r2, #40
	cmp r0, r1		; Testa condicoes de Contorno 
	jle MoveBird_RecalculaPos_Fim
	sub r0, r0, r2	; pos = pos - 40
	jmp MoveBird_RecalculaPos_Fim

;----------------------------------
MoveBird_Desenha:	; Desenha caractere da Bird
	push r0
	push r1
	
	loadn r1, #'>'	; Bird
	load r0, posBird
	outchar r1, r0
	store posAntBird, r0	; Atualiza Posicao Anterior da Bird = Posicao Atual
	
	pop r1
	pop r0
	rts


Colisao:
		push r0
		push r1
		push r2
		push r3
		
		load r0, posBird
		loadn r1,#pipe	   ; Checa colisão com as paredes
		loadn r2,#'|'
		add r1,r1,r0
		loadi r3,r1
		outchar r3,r0
		cmp r2,r3
		jeq Gameover
		
		loadn r2, #662
		cmp r2,r0
		ceq score
		
		loadn r2, #622
		cmp r2,r0
		ceq score
		
		loadn r2, #582
		cmp r2,r0
		ceq score
		
		loadn r2, #542
		cmp r2,r0
		ceq score
			
		pop r0
		pop r1
		pop r2
		pop r3	   
		
		rts
		
	
;********************************************************
;                       DELAY
;********************************************************	
	
	
Delay:
						;Utiliza Push e Pop para nao afetar os ristradores do programa principal
	push r0
	push r1
	
	loadn r1, #10  ; a
   Delay_volta2:				;Quebrou o contador acima em duas partes (dois loops de decremento)
	loadn r0, #3000	; b
   Delay_volta: 
	dec r0					; (4*a + 6)b = 1000000  == 1 seg  em um clock de 1MHz
	jnz Delay_volta	
	dec r1
	jnz Delay_volta2
	
	pop r1
	pop r0
	rts							;return

;-------------------------------


;********************************************************
;                       IMPrIME TELA
;********************************************************	

ImprimeTela: 	;  rotina de Impresao de Cenario na Tela Inteira
		;  r1 = endereco onde comeca a primeira linha do Cenario
		;  r2 = cor do Cenario para ser impresso

	push r0	; protege o r3 na pilha para ser usado na subrotina
	push r1	; protege o r1 na pilha para preservar seu valor
	push r2	; protege o r1 na pilha para preservar seu valor
	push r3	; protege o r3 na pilha para ser usado na subrotina
	push r4	; protege o r4 na pilha para ser usado na subrotina
	push r5	; protege o r4 na pilha para ser usado na subrotina

	loadn r0, #0  	; posicao inicial tem que ser o comeco da tela!
	loadn r3, #40  	; Incremento da posicao da tela!
	loadn r4, #41  	; incremento do ponteiro das linhas da tela
	loadn r5, #1200 ; Limite da tela!
	
   ImprimeTela_Loop:
		call ImprimeStr
		add r0, r0, r3  	; incrementaposicao para a segunda linha na tela -->  r0 = r0 + 40
		add r1, r1, r4  	; incrementa o ponteiro para o comeco da proxima linha na memoria (40 + 1 porcausa do /0 !!) --> r1 = r1 + 41
		cmp r0, r5			; Compara r0 com 1200
		jne ImprimeTela_Loop	; Enquanto r0 < 1200

	pop r5	; resgata os valores dos registradores utilizados na Subrotina da Pilha
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	rts
				
;---------------------

;---------------------------	
;********************************************************
;                   IMPrIME STrING
;********************************************************
	
ImprimeStr:	;  rotina de Impresao de Mensagens:    r0 = Posicao da tela que o primeiro caractere da mensagem sera' impresso;  r1 = endereco onde comeca a mensagem; r2 = cor da mensagem.   Obs: a mensagem sera' impressa ate' encontrar "/0"
	push r0	; protege o r0 na pilha para preservar seu valor
	push r1	; protege o r1 na pilha para preservar seu valor
	push r2	; protege o r1 na pilha para preservar seu valor
	push r3	; protege o r3 na pilha para ser usado na subrotina
	push r4	; protege o r4 na pilha para ser usado na subrotina
	
	loadn r3, #'\0'	; Criterio de parada

   ImprimeStr_Loop:	
		loadi r4, r1
		cmp r4, r3		; If (Char == \0)  vai Embora
		jeq ImprimeStr_Sai
		add r4, r2, r4	; Soma a Cor
		outchar r4, r0	; Imprime o caractere na tela
		inc r0			; Incrementa a posicao na tela
		inc r1			; Incrementa o ponteiro da String
		jmp ImprimeStr_Loop
	
   ImprimeStr_Sai:	
	pop r4	; resgata os valores dos registradores utilizados na Subrotina da Pilha
	pop r3
	pop r2
	pop r1
	pop r0
	rts
	
;------------------------	

Gameover:
	call ApagaTela
	loadn r1, #tela1Linha0	; Endereco onde comeca a primeira linha do cenario!!
	loadn r2, #0  			; cor branca!
	call ImprimeTela
	
	halt
		
	
;********************************************************
;                       APAGA TELA
;********************************************************
ApagaTela:
	push r0
	push r1
	
	loadn r0, #1200		; apaga as 1200 posicoes da Tela
	loadn r1, #' '		; com "espaco"
	
	   ApagaTela_Loop:	;;label for(r0=1200;r3>0;r3--)
		dec r0
		outchar r1, r0
		jnz ApagaTela_Loop
 
	pop r1
	pop r0
	rts	
	
;------------------------	

;-------------------------
score:
		push r0
		push r1
		
		load r0, scoreunidade	; Soma um ponto na unidade
		loadn r1, #'9'			; Se a unidade ja for 9, então vai pra somar na dezena
		cmp r1, r0
		jeq somadezena
		
		inc r0  
		store scoreunidade, r0
		
		jmp atualizascore
		
somadezena: 					; Soma um na dezena
		loadn r0, #'0'			
		store scoreunidade, r0
		
		load r0, scoredezena
		loadn r1, #'9'			; Se a dezena já é 9, então vai somar na centena
		cmp r1, r0
		jeq somacentena
		
		inc r0  
		store scoredezena, r0
		
		jmp atualizascore
		
somacentena:					; Soma um na centena
		loadn r0, #'0'
		store scoredezena, r0
		
		load r0, scorecentena
		loadn r1, #'9'			; Se a centena é 9, então da gameover, é impossível chegar a pontuação de 900 ja que a cobra tem tamanho maximo de 100
		cmp r1, r0
		
		inc r0  
		store scorecentena, r0
		
atualizascore:					; reescreve o score na tela para atualizar a pontuação
		load r0,scoreunidade
		load r1,posicaounidade
		outchar r0,r1
		
		load r0,scoredezena
		load r1,posicaodezena
		outchar r0,r1
		
		load r0,scorecentena
		load r1,posicaocentena
		outchar r0,r1
		
		pop r0
		pop r1		
		rts
		
		
		
; Declara uma tela vazia para ser preenchida em tempo de execussao:
tela0Linha0  : string "                                        "
tela0Linha1  : string "                                        "
tela0Linha2  : string "                                        "
tela0Linha3  : string "                                        "
tela0Linha4  : string "                                        "
tela0Linha5  : string "                                        "
tela0Linha6  : string "                                        "
tela0Linha7  : string "                                        "
tela0Linha8  : string "                                        "
tela0Linha9  : string "                                        "
tela0Linha10 : string "                                        "
tela0Linha11 : string "                                        "
tela0Linha12 : string "                                        "
tela0Linha13 : string "                                        "
tela0Linha14 : string "                                        "
tela0Linha15 : string "                                        "
tela0Linha16 : string "                                        "
tela0Linha17 : string "                                        "
tela0Linha18 : string "                                        "
tela0Linha19 : string "                                        "
tela0Linha20 : string "                                        "
tela0Linha21 : string "                                        "
tela0Linha22 : string "                                        "
tela0Linha23 : string "                                        "
tela0Linha24 : string "                                        "
tela0Linha25 : string "                                        "
tela0Linha26 : string "                                        "
tela0Linha27 : string "                                        "
tela0Linha28 : string "                                        "
tela0Linha29 : string "                                        "

;Gameover
tela1Linha0  : string "                                        "
tela1Linha1  : string "                                        "
tela1Linha2  : string "                                        "
tela1Linha3  : string "                                        "
tela1Linha4  : string "                                        "
tela1Linha5  : string "                                        "
tela1Linha6  : string "                                        "
tela1Linha7  : string "                                        "
tela1Linha8  : string "                                        "
tela1Linha9  : string "                                        "
tela1Linha10 : string "                                        "
tela1Linha11 : string "                                        "
tela1Linha12 : string "                                        "
tela1Linha13 : string "                                        "
tela1Linha14 : string "                GAMEOVER                "
tela1Linha15 : string "                                        "
tela1Linha16 : string "                                        "
tela1Linha17 : string "                                        "
tela1Linha18 : string "                                        "
tela1Linha19 : string "                                        "
tela1Linha20 : string "                                        "
tela1Linha21 : string "                                        "
tela1Linha22 : string "                                        "
tela1Linha23 : string "                                        "
tela1Linha24 : string "                                        "
tela1Linha25 : string "                                        "
tela1Linha26 : string "                                        "
tela1Linha27 : string "                                        "
tela1Linha28 : string "                                        "
tela1Linha29 : string "                                        "


pipe : var #1200
static pipe + #0, #' '
static pipe + #1, #' '
static pipe + #2, #' '
static pipe + #3, #' '
static pipe + #4, #' '
static pipe + #5, #' '
static pipe + #6, #' '
static pipe + #7, #' '
static pipe + #8, #' '
static pipe + #9, #' '
static pipe + #10, #' '
static pipe + #11, #' '
static pipe + #12, #' '
static pipe + #13, #' '
static pipe + #14, #' '
static pipe + #15, #' '
static pipe + #16, #' '
static pipe + #17, #' '
static pipe + #18, #' '
static pipe + #19, #' '
static pipe + #20, #' '
static pipe + #21, #' '
static pipe + #22, #' '
static pipe + #23, #' '
static pipe + #24, #' '
static pipe + #25, #' '
static pipe + #26, #' '
static pipe + #27, #' '
static pipe + #28, #' '
static pipe + #29, #' '
static pipe + #30, #' '
static pipe + #31, #' '
static pipe + #32, #' '
static pipe + #33, #' '
static pipe + #34, #' '
static pipe + #35, #' '
static pipe + #36, #' '
static pipe + #37, #' '
static pipe + #38, #' '
static pipe + #39, #' '
static pipe + #40, #' '
static pipe + #41, #' '
static pipe + #42, #'F'
static pipe + #43, #'L'
static pipe + #44, #'A'
static pipe + #45, #'P'
static pipe + #46, #'P'
static pipe + #47, #'Y'
static pipe + #48, #' '
static pipe + #49, #' '
static pipe + #50, #' '
static pipe + #51, #' '
static pipe + #52, #' '
static pipe + #53, #' '
static pipe + #54, #' '
static pipe + #55, #' '
static pipe + #56, #' '
static pipe + #57, #' '
static pipe + #58, #' '
static pipe + #59, #' '
static pipe + #60, #' '
static pipe + #61, #' '
static pipe + #62, #' '
static pipe + #63, #' '
static pipe + #64, #' '
static pipe + #65, #' '
static pipe + #66, #' '
static pipe + #67, #' '
static pipe + #68, #' '
static pipe + #69, #'S'
static pipe + #70, #'C'
static pipe + #71, #'O'
static pipe + #72, #'R'
static pipe + #73, #'E'
static pipe + #74, #' '
static pipe + #75, #'0'
static pipe + #76, #'0'
static pipe + #77, #'0'
static pipe + #78, #' '
static pipe + #79, #' '
static pipe + #80, #' '
static pipe + #81, #' '
static pipe + #82, #' '
static pipe + #83, #' '
static pipe + #84, #' '
static pipe + #85, #' '
static pipe + #86, #' '
static pipe + #87, #' '
static pipe + #88, #' '
static pipe + #89, #' '
static pipe + #90, #' '
static pipe + #91, #' '
static pipe + #92, #' '
static pipe + #93, #' '
static pipe + #94, #' '
static pipe + #95, #' '
static pipe + #96, #' '
static pipe + #97, #' '
static pipe + #98, #' '
static pipe + #99, #' '
static pipe + #100, #'|'
static pipe + #101, #'|'
static pipe + #102, #' '
static pipe + #103, #' '
static pipe + #104, #' '
static pipe + #105, #' '
static pipe + #106, #' '
static pipe + #107, #' '
static pipe + #108, #' '
static pipe + #109, #' '
static pipe + #110, #' '
static pipe + #111, #' '
static pipe + #112, #' '
static pipe + #113, #' '
static pipe + #114, #' '
static pipe + #115, #' '
static pipe + #116, #' '
static pipe + #117, #' '
static pipe + #118, #' '
static pipe + #119, #' '
static pipe + #120, #' '
static pipe + #121, #' '
static pipe + #122, #' '
static pipe + #123, #' '
static pipe + #124, #' '
static pipe + #125, #' '
static pipe + #126, #' '
static pipe + #127, #' '
static pipe + #128, #' '
static pipe + #129, #' '
static pipe + #130, #' '
static pipe + #131, #' '
static pipe + #132, #' '
static pipe + #133, #' '
static pipe + #134, #' '
static pipe + #135, #' '
static pipe + #136, #' '
static pipe + #137, #' '
static pipe + #138, #' '
static pipe + #139, #' '
static pipe + #140, #'|'
static pipe + #141, #'|'
static pipe + #142, #' '
static pipe + #143, #' '
static pipe + #144, #' '
static pipe + #145, #' '
static pipe + #146, #' '
static pipe + #147, #' '
static pipe + #148, #' '
static pipe + #149, #' '
static pipe + #150, #' '
static pipe + #151, #' '
static pipe + #152, #' '
static pipe + #153, #' '
static pipe + #154, #' '
static pipe + #155, #' '
static pipe + #156, #' '
static pipe + #157, #' '
static pipe + #158, #' '
static pipe + #159, #' '
static pipe + #160, #' '
static pipe + #161, #' '
static pipe + #162, #' '
static pipe + #163, #' '
static pipe + #164, #' '
static pipe + #165, #' '
static pipe + #166, #' '
static pipe + #167, #' '
static pipe + #168, #' '
static pipe + #169, #' '
static pipe + #170, #' '
static pipe + #171, #' '
static pipe + #172, #' '
static pipe + #173, #' '
static pipe + #174, #' '
static pipe + #175, #' '
static pipe + #176, #' '
static pipe + #177, #' '
static pipe + #178, #' '
static pipe + #179, #' '
static pipe + #180, #'|'
static pipe + #181, #'|'
static pipe + #182, #' '
static pipe + #183, #' '
static pipe + #184, #' '
static pipe + #185, #' '
static pipe + #186, #' '
static pipe + #187, #' '
static pipe + #188, #' '
static pipe + #189, #' '
static pipe + #190, #' '
static pipe + #191, #' '
static pipe + #192, #' '
static pipe + #193, #' '
static pipe + #194, #' '
static pipe + #195, #' '
static pipe + #196, #' '
static pipe + #197, #' '
static pipe + #198, #' '
static pipe + #199, #' '
static pipe + #200, #' '
static pipe + #201, #' '
static pipe + #202, #' '
static pipe + #203, #' '
static pipe + #204, #' '
static pipe + #205, #' '
static pipe + #206, #' '
static pipe + #207, #' '
static pipe + #208, #' '
static pipe + #209, #' '
static pipe + #210, #' '
static pipe + #211, #' '
static pipe + #212, #' '
static pipe + #213, #' '
static pipe + #214, #' '
static pipe + #215, #' '
static pipe + #216, #' '
static pipe + #217, #' '
static pipe + #218, #' '
static pipe + #219, #' '
static pipe + #220, #'|'
static pipe + #221, #'|'
static pipe + #222, #' '
static pipe + #223, #' '
static pipe + #224, #' '
static pipe + #225, #' '
static pipe + #226, #' '
static pipe + #227, #' '
static pipe + #228, #' '
static pipe + #229, #' '
static pipe + #230, #' '
static pipe + #231, #' '
static pipe + #232, #' '
static pipe + #233, #' '
static pipe + #234, #' '
static pipe + #235, #' '
static pipe + #236, #' '
static pipe + #237, #' '
static pipe + #238, #' '
static pipe + #239, #' '
static pipe + #240, #' '
static pipe + #241, #' '
static pipe + #242, #' '
static pipe + #243, #' '
static pipe + #244, #' '
static pipe + #245, #' '
static pipe + #246, #' '
static pipe + #247, #' '
static pipe + #248, #' '
static pipe + #249, #' '
static pipe + #250, #' '
static pipe + #251, #' '
static pipe + #252, #' '
static pipe + #253, #' '
static pipe + #254, #' '
static pipe + #255, #' '
static pipe + #256, #' '
static pipe + #257, #' '
static pipe + #258, #' '
static pipe + #259, #' '
static pipe + #260, #'|'
static pipe + #261, #'|'
static pipe + #262, #' '
static pipe + #263, #' '
static pipe + #264, #' '
static pipe + #265, #' '
static pipe + #266, #' '
static pipe + #267, #' '
static pipe + #268, #' '
static pipe + #269, #' '
static pipe + #270, #' '
static pipe + #271, #' '
static pipe + #272, #' '
static pipe + #273, #' '
static pipe + #274, #' '
static pipe + #275, #' '
static pipe + #276, #' '
static pipe + #277, #' '
static pipe + #278, #' '
static pipe + #279, #' '
static pipe + #280, #' '
static pipe + #281, #' '
static pipe + #282, #' '
static pipe + #283, #' '
static pipe + #284, #' '
static pipe + #285, #' '
static pipe + #286, #' '
static pipe + #287, #' '
static pipe + #288, #' '
static pipe + #289, #' '
static pipe + #290, #' '
static pipe + #291, #' '
static pipe + #292, #' '
static pipe + #293, #' '
static pipe + #294, #' '
static pipe + #295, #' '
static pipe + #296, #' '
static pipe + #297, #' '
static pipe + #298, #' '
static pipe + #299, #' '
static pipe + #300, #'|'
static pipe + #301, #'|'
static pipe + #302, #' '
static pipe + #303, #' '
static pipe + #304, #' '
static pipe + #305, #' '
static pipe + #306, #' '
static pipe + #307, #' '
static pipe + #308, #' '
static pipe + #309, #' '
static pipe + #310, #' '
static pipe + #311, #' '
static pipe + #312, #' '
static pipe + #313, #' '
static pipe + #314, #' '
static pipe + #315, #' '
static pipe + #316, #' '
static pipe + #317, #' '
static pipe + #318, #' '
static pipe + #319, #' '
static pipe + #320, #' '
static pipe + #321, #' '
static pipe + #322, #' '
static pipe + #323, #' '
static pipe + #324, #' '
static pipe + #325, #' '
static pipe + #326, #' '
static pipe + #327, #' '
static pipe + #328, #' '
static pipe + #329, #' '
static pipe + #330, #' '
static pipe + #331, #' '
static pipe + #332, #' '
static pipe + #333, #' '
static pipe + #334, #' '
static pipe + #335, #' '
static pipe + #336, #' '
static pipe + #337, #' '
static pipe + #338, #' '
static pipe + #339, #' '
static pipe + #340, #'|'
static pipe + #341, #'|'
static pipe + #342, #' '
static pipe + #343, #' '
static pipe + #344, #' '
static pipe + #345, #' '
static pipe + #346, #' '
static pipe + #347, #' '
static pipe + #348, #' '
static pipe + #349, #' '
static pipe + #350, #' '
static pipe + #351, #' '
static pipe + #352, #' '
static pipe + #353, #' '
static pipe + #354, #' '
static pipe + #355, #' '
static pipe + #356, #' '
static pipe + #357, #' '
static pipe + #358, #' '
static pipe + #359, #' '
static pipe + #360, #' '
static pipe + #361, #' '
static pipe + #362, #' '
static pipe + #363, #' '
static pipe + #364, #' '
static pipe + #365, #' '
static pipe + #366, #' '
static pipe + #367, #' '
static pipe + #368, #' '
static pipe + #369, #' '
static pipe + #370, #' '
static pipe + #371, #' '
static pipe + #372, #' '
static pipe + #373, #' '
static pipe + #374, #' '
static pipe + #375, #' '
static pipe + #376, #' '
static pipe + #377, #' '
static pipe + #378, #' '
static pipe + #379, #' '
static pipe + #380, #'|'
static pipe + #381, #'|'
static pipe + #382, #' '
static pipe + #383, #' '
static pipe + #384, #' '
static pipe + #385, #' '
static pipe + #386, #' '
static pipe + #387, #' '
static pipe + #388, #' '
static pipe + #389, #' '
static pipe + #390, #' '
static pipe + #391, #' '
static pipe + #392, #' '
static pipe + #393, #' '
static pipe + #394, #' '
static pipe + #395, #' '
static pipe + #396, #' '
static pipe + #397, #' '
static pipe + #398, #' '
static pipe + #399, #' '
static pipe + #400, #' '
static pipe + #401, #' '
static pipe + #402, #' '
static pipe + #403, #' '
static pipe + #404, #' '
static pipe + #405, #' '
static pipe + #406, #' '
static pipe + #407, #' '
static pipe + #408, #' '
static pipe + #409, #' '
static pipe + #410, #' '
static pipe + #411, #' '
static pipe + #412, #' '
static pipe + #413, #' '
static pipe + #414, #' '
static pipe + #415, #' '
static pipe + #416, #' '
static pipe + #417, #' '
static pipe + #418, #' '
static pipe + #419, #' '
static pipe + #420, #'|'
static pipe + #421, #'|'
static pipe + #422, #' '
static pipe + #423, #' '
static pipe + #424, #' '
static pipe + #425, #' '
static pipe + #426, #' '
static pipe + #427, #' '
static pipe + #428, #' '
static pipe + #429, #' '
static pipe + #430, #' '
static pipe + #431, #' '
static pipe + #432, #' '
static pipe + #433, #' '
static pipe + #434, #' '
static pipe + #435, #' '
static pipe + #436, #' '
static pipe + #437, #' '
static pipe + #438, #' '
static pipe + #439, #' '
static pipe + #440, #' '
static pipe + #441, #' '
static pipe + #442, #' '
static pipe + #443, #' '
static pipe + #444, #' '
static pipe + #445, #' '
static pipe + #446, #' '
static pipe + #447, #' '
static pipe + #448, #' '
static pipe + #449, #' '
static pipe + #450, #' '
static pipe + #451, #' '
static pipe + #452, #' '
static pipe + #453, #' '
static pipe + #454, #' '
static pipe + #455, #' '
static pipe + #456, #' '
static pipe + #457, #' '
static pipe + #458, #' '
static pipe + #459, #' '
static pipe + #460, #'|'
static pipe + #461, #'|'
static pipe + #462, #' '
static pipe + #463, #' '
static pipe + #464, #' '
static pipe + #465, #' '
static pipe + #466, #' '
static pipe + #467, #' '
static pipe + #468, #' '
static pipe + #469, #' '
static pipe + #470, #' '
static pipe + #471, #' '
static pipe + #472, #' '
static pipe + #473, #' '
static pipe + #474, #' '
static pipe + #475, #' '
static pipe + #476, #' '
static pipe + #477, #' '
static pipe + #478, #' '
static pipe + #479, #' '
static pipe + #480, #' '
static pipe + #481, #' '
static pipe + #482, #' '
static pipe + #483, #' '
static pipe + #484, #' '
static pipe + #485, #' '
static pipe + #486, #' '
static pipe + #487, #' '
static pipe + #488, #' '
static pipe + #489, #' '
static pipe + #490, #' '
static pipe + #491, #' '
static pipe + #492, #' '
static pipe + #493, #' '
static pipe + #494, #' '
static pipe + #495, #' '
static pipe + #496, #' '
static pipe + #497, #' '
static pipe + #498, #' '
static pipe + #499, #' '
static pipe + #500, #'|'
static pipe + #501, #'|'
static pipe + #502, #' '
static pipe + #503, #' '
static pipe + #504, #' '
static pipe + #505, #' '
static pipe + #506, #' '
static pipe + #507, #' '
static pipe + #508, #' '
static pipe + #509, #' '
static pipe + #510, #' '
static pipe + #511, #' '
static pipe + #512, #' '
static pipe + #513, #' '
static pipe + #514, #' '
static pipe + #515, #' '
static pipe + #516, #' '
static pipe + #517, #' '
static pipe + #518, #' '
static pipe + #519, #' '
static pipe + #520, #' '
static pipe + #521, #' '
static pipe + #522, #' '
static pipe + #523, #' '
static pipe + #524, #' '
static pipe + #525, #' '
static pipe + #526, #' '
static pipe + #527, #' '
static pipe + #528, #' '
static pipe + #529, #' '
static pipe + #530, #' '
static pipe + #531, #' '
static pipe + #532, #' '
static pipe + #533, #' '
static pipe + #534, #' '
static pipe + #535, #' '
static pipe + #536, #' '
static pipe + #537, #' '
static pipe + #538, #' '
static pipe + #539, #' '
static pipe + #540, #' '
static pipe + #541, #' '
static pipe + #542, #' '
static pipe + #543, #' '
static pipe + #544, #' '
static pipe + #545, #' '
static pipe + #546, #' '
static pipe + #547, #' '
static pipe + #548, #' '
static pipe + #549, #' '
static pipe + #550, #' '
static pipe + #551, #' '
static pipe + #552, #' '
static pipe + #553, #' '
static pipe + #554, #' '
static pipe + #555, #' '
static pipe + #556, #' '
static pipe + #557, #' '
static pipe + #558, #' '
static pipe + #559, #' '
static pipe + #560, #' '
static pipe + #561, #' '
static pipe + #562, #' '
static pipe + #563, #' '
static pipe + #564, #' '
static pipe + #565, #' '
static pipe + #566, #' '
static pipe + #567, #' '
static pipe + #568, #' '
static pipe + #569, #' '
static pipe + #570, #' '
static pipe + #571, #' '
static pipe + #572, #' '
static pipe + #573, #' '
static pipe + #574, #' '
static pipe + #575, #' '
static pipe + #576, #' '
static pipe + #577, #' '
static pipe + #578, #' '
static pipe + #579, #' '
static pipe + #580, #' '
static pipe + #581, #' '
static pipe + #582, #' '
static pipe + #583, #' '
static pipe + #584, #' '
static pipe + #585, #' '
static pipe + #586, #' '
static pipe + #587, #' '
static pipe + #588, #' '
static pipe + #589, #' '
static pipe + #590, #' '
static pipe + #591, #' '
static pipe + #592, #' '
static pipe + #593, #' '
static pipe + #594, #' '
static pipe + #595, #' '
static pipe + #596, #' '
static pipe + #597, #' '
static pipe + #598, #' '
static pipe + #599, #' '
static pipe + #600, #' '
static pipe + #601, #' '
static pipe + #602, #' '
static pipe + #603, #' '
static pipe + #604, #' '
static pipe + #605, #' '
static pipe + #606, #' '
static pipe + #607, #' '
static pipe + #608, #' '
static pipe + #609, #' '
static pipe + #610, #' '
static pipe + #611, #' '
static pipe + #612, #' '
static pipe + #613, #' '
static pipe + #614, #' '
static pipe + #615, #' '
static pipe + #616, #' '
static pipe + #617, #' '
static pipe + #618, #' '
static pipe + #619, #' '
static pipe + #620, #' '
static pipe + #621, #' '
static pipe + #622, #' '
static pipe + #623, #' '
static pipe + #624, #' '
static pipe + #625, #' '
static pipe + #626, #' '
static pipe + #627, #' '
static pipe + #628, #' '
static pipe + #629, #' '
static pipe + #630, #' '
static pipe + #631, #' '
static pipe + #632, #' '
static pipe + #633, #' '
static pipe + #634, #' '
static pipe + #635, #' '
static pipe + #636, #' '
static pipe + #637, #' '
static pipe + #638, #' '
static pipe + #639, #' '
static pipe + #640, #' '
static pipe + #641, #' '
static pipe + #642, #' '
static pipe + #643, #' '
static pipe + #644, #' '
static pipe + #645, #' '
static pipe + #646, #' '
static pipe + #647, #' '
static pipe + #648, #' '
static pipe + #649, #' '
static pipe + #650, #' '
static pipe + #651, #' '
static pipe + #652, #' '
static pipe + #653, #' '
static pipe + #654, #' '
static pipe + #655, #' '
static pipe + #656, #' '
static pipe + #657, #' '
static pipe + #658, #' '
static pipe + #659, #' '
static pipe + #660, #'|'
static pipe + #661, #'|'
static pipe + #662, #' '
static pipe + #663, #' '
static pipe + #664, #' '
static pipe + #665, #' '
static pipe + #666, #' '
static pipe + #667, #' '
static pipe + #668, #' '
static pipe + #669, #' '
static pipe + #670, #' '
static pipe + #671, #' '
static pipe + #672, #' '
static pipe + #673, #' '
static pipe + #674, #' '
static pipe + #675, #' '
static pipe + #676, #' '
static pipe + #677, #' '
static pipe + #678, #' '
static pipe + #679, #' '
static pipe + #680, #' '
static pipe + #681, #' '
static pipe + #682, #' '
static pipe + #683, #' '
static pipe + #684, #' '
static pipe + #685, #' '
static pipe + #686, #' '
static pipe + #687, #' '
static pipe + #688, #' '
static pipe + #689, #' '
static pipe + #690, #' '
static pipe + #691, #' '
static pipe + #692, #' '
static pipe + #693, #' '
static pipe + #694, #' '
static pipe + #695, #' '
static pipe + #696, #' '
static pipe + #697, #' '
static pipe + #698, #' '
static pipe + #699, #' '
static pipe + #700, #'|'
static pipe + #701, #'|'
static pipe + #702, #' '
static pipe + #703, #' '
static pipe + #704, #' '
static pipe + #705, #' '
static pipe + #706, #' '
static pipe + #707, #' '
static pipe + #708, #' '
static pipe + #709, #' '
static pipe + #710, #' '
static pipe + #711, #' '
static pipe + #712, #' '
static pipe + #713, #' '
static pipe + #714, #' '
static pipe + #715, #' '
static pipe + #716, #' '
static pipe + #717, #' '
static pipe + #718, #' '
static pipe + #719, #' '
static pipe + #720, #' '
static pipe + #721, #' '
static pipe + #722, #' '
static pipe + #723, #' '
static pipe + #724, #' '
static pipe + #725, #' '
static pipe + #726, #' '
static pipe + #727, #' '
static pipe + #728, #' '
static pipe + #729, #' '
static pipe + #730, #' '
static pipe + #731, #' '
static pipe + #732, #' '
static pipe + #733, #' '
static pipe + #734, #' '
static pipe + #735, #' '
static pipe + #736, #' '
static pipe + #737, #' '
static pipe + #738, #' '
static pipe + #739, #' '
static pipe + #740, #'|'
static pipe + #741, #'|'
static pipe + #742, #' '
static pipe + #743, #' '
static pipe + #744, #' '
static pipe + #745, #' '
static pipe + #746, #' '
static pipe + #747, #' '
static pipe + #748, #' '
static pipe + #749, #' '
static pipe + #750, #' '
static pipe + #751, #' '
static pipe + #752, #' '
static pipe + #753, #' '
static pipe + #754, #' '
static pipe + #755, #' '
static pipe + #756, #' '
static pipe + #757, #' '
static pipe + #758, #' '
static pipe + #759, #' '
static pipe + #760, #' '
static pipe + #761, #' '
static pipe + #762, #' '
static pipe + #763, #' '
static pipe + #764, #' '
static pipe + #765, #' '
static pipe + #766, #' '
static pipe + #767, #' '
static pipe + #768, #' '
static pipe + #769, #' '
static pipe + #770, #' '
static pipe + #771, #' '
static pipe + #772, #' '
static pipe + #773, #' '
static pipe + #774, #' '
static pipe + #775, #' '
static pipe + #776, #' '
static pipe + #777, #' '
static pipe + #778, #' '
static pipe + #779, #' '
static pipe + #780, #'|'
static pipe + #781, #'|'
static pipe + #782, #' '
static pipe + #783, #' '
static pipe + #784, #' '
static pipe + #785, #' '
static pipe + #786, #' '
static pipe + #787, #' '
static pipe + #788, #' '
static pipe + #789, #' '
static pipe + #790, #' '
static pipe + #791, #' '
static pipe + #792, #' '
static pipe + #793, #' '
static pipe + #794, #' '
static pipe + #795, #' '
static pipe + #796, #' '
static pipe + #797, #' '
static pipe + #798, #' '
static pipe + #799, #' '
static pipe + #800, #' '
static pipe + #801, #' '
static pipe + #802, #' '
static pipe + #803, #' '
static pipe + #804, #' '
static pipe + #805, #' '
static pipe + #806, #' '
static pipe + #807, #' '
static pipe + #808, #' '
static pipe + #809, #' '
static pipe + #810, #' '
static pipe + #811, #' '
static pipe + #812, #' '
static pipe + #813, #' '
static pipe + #814, #' '
static pipe + #815, #' '
static pipe + #816, #' '
static pipe + #817, #' '
static pipe + #818, #' '
static pipe + #819, #' '
static pipe + #820, #'|'
static pipe + #821, #'|'
static pipe + #822, #' '
static pipe + #823, #' '
static pipe + #824, #' '
static pipe + #825, #' '
static pipe + #826, #' '
static pipe + #827, #' '
static pipe + #828, #' '
static pipe + #829, #' '
static pipe + #830, #' '
static pipe + #831, #' '
static pipe + #832, #' '
static pipe + #833, #' '
static pipe + #834, #' '
static pipe + #835, #' '
static pipe + #836, #' '
static pipe + #837, #' '
static pipe + #838, #' '
static pipe + #839, #' '
static pipe + #840, #' '
static pipe + #841, #' '
static pipe + #842, #' '
static pipe + #843, #' '
static pipe + #844, #' '
static pipe + #845, #' '
static pipe + #846, #' '
static pipe + #847, #' '
static pipe + #848, #' '
static pipe + #849, #' '
static pipe + #850, #' '
static pipe + #851, #' '
static pipe + #852, #' '
static pipe + #853, #' '
static pipe + #854, #' '
static pipe + #855, #' '
static pipe + #856, #' '
static pipe + #857, #' '
static pipe + #858, #' '
static pipe + #859, #' '
static pipe + #860, #'|'
static pipe + #861, #'|'
static pipe + #862, #' '
static pipe + #863, #' '
static pipe + #864, #' '
static pipe + #865, #' '
static pipe + #866, #' '
static pipe + #867, #' '
static pipe + #868, #' '
static pipe + #869, #' '
static pipe + #870, #' '
static pipe + #871, #' '
static pipe + #872, #' '
static pipe + #873, #' '
static pipe + #874, #' '
static pipe + #875, #' '
static pipe + #876, #' '
static pipe + #877, #' '
static pipe + #878, #' '
static pipe + #879, #' '
static pipe + #880, #' '
static pipe + #881, #' '
static pipe + #882, #' '
static pipe + #883, #' '
static pipe + #884, #' '
static pipe + #885, #' '
static pipe + #886, #' '
static pipe + #887, #' '
static pipe + #888, #' '
static pipe + #889, #' '
static pipe + #890, #' '
static pipe + #891, #' '
static pipe + #892, #' '
static pipe + #893, #' '
static pipe + #894, #' '
static pipe + #895, #' '
static pipe + #896, #' '
static pipe + #897, #' '
static pipe + #898, #' '
static pipe + #899, #' '
static pipe + #900, #'|'
static pipe + #901, #'|'
static pipe + #902, #' '
static pipe + #903, #' '
static pipe + #904, #' '
static pipe + #905, #' '
static pipe + #906, #' '
static pipe + #907, #' '
static pipe + #908, #' '
static pipe + #909, #' '
static pipe + #910, #' '
static pipe + #911, #' '
static pipe + #912, #' '
static pipe + #913, #' '
static pipe + #914, #' '
static pipe + #915, #' '
static pipe + #916, #' '
static pipe + #917, #' '
static pipe + #918, #' '
static pipe + #919, #' '
static pipe + #920, #' '
static pipe + #921, #' '
static pipe + #922, #' '
static pipe + #923, #' '
static pipe + #924, #' '
static pipe + #925, #' '
static pipe + #926, #' '
static pipe + #927, #' '
static pipe + #928, #' '
static pipe + #929, #' '
static pipe + #930, #' '
static pipe + #931, #' '
static pipe + #932, #' '
static pipe + #933, #' '
static pipe + #934, #' '
static pipe + #935, #' '
static pipe + #936, #' '
static pipe + #937, #' '
static pipe + #938, #' '
static pipe + #939, #' '
static pipe + #940, #'|'
static pipe + #941, #'|'
static pipe + #942, #' '
static pipe + #943, #' '
static pipe + #944, #' '
static pipe + #945, #' '
static pipe + #946, #' '
static pipe + #947, #' '
static pipe + #948, #' '
static pipe + #949, #' '
static pipe + #950, #' '
static pipe + #951, #' '
static pipe + #952, #' '
static pipe + #953, #' '
static pipe + #954, #' '
static pipe + #955, #' '
static pipe + #956, #' '
static pipe + #957, #' '
static pipe + #958, #' '
static pipe + #959, #' '
static pipe + #960, #' '
static pipe + #961, #' '
static pipe + #962, #' '
static pipe + #963, #' '
static pipe + #964, #' '
static pipe + #965, #' '
static pipe + #966, #' '
static pipe + #967, #' '
static pipe + #968, #' '
static pipe + #969, #' '
static pipe + #970, #' '
static pipe + #971, #' '
static pipe + #972, #' '
static pipe + #973, #' '
static pipe + #974, #' '
static pipe + #975, #' '
static pipe + #976, #' '
static pipe + #977, #' '
static pipe + #978, #' '
static pipe + #979, #' '
static pipe + #980, #'|'
static pipe + #981, #'|'
static pipe + #982, #' '
static pipe + #983, #' '
static pipe + #984, #' '
static pipe + #985, #' '
static pipe + #986, #' '
static pipe + #987, #' '
static pipe + #988, #' '
static pipe + #989, #' '
static pipe + #990, #' '
static pipe + #991, #' '
static pipe + #992, #' '
static pipe + #993, #' '
static pipe + #994, #' '
static pipe + #995, #' '
static pipe + #996, #' '
static pipe + #997, #' '
static pipe + #998, #' '
static pipe + #999, #' '
static pipe + #1000, #' '
static pipe + #1001, #' '
static pipe + #1002, #' '
static pipe + #1003, #' '
static pipe + #1004, #' '
static pipe + #1005, #' '
static pipe + #1006, #' '
static pipe + #1007, #' '
static pipe + #1008, #' '
static pipe + #1009, #' '
static pipe + #1010, #' '
static pipe + #1011, #' '
static pipe + #1012, #' '
static pipe + #1013, #' '
static pipe + #1014, #' '
static pipe + #1015, #' '
static pipe + #1016, #' '
static pipe + #1017, #' '
static pipe + #1018, #' '
static pipe + #1019, #' '
static pipe + #1020, #'|'
static pipe + #1021, #'|'
static pipe + #1022, #' '
static pipe + #1023, #' '
static pipe + #1024, #' '
static pipe + #1025, #' '
static pipe + #1026, #' '
static pipe + #1027, #' '
static pipe + #1028, #' '
static pipe + #1029, #' '
static pipe + #1030, #' '
static pipe + #1031, #' '
static pipe + #1032, #' '
static pipe + #1033, #' '
static pipe + #1034, #' '
static pipe + #1035, #' '
static pipe + #1036, #' '
static pipe + #1037, #' '
static pipe + #1038, #' '
static pipe + #1039, #' '
static pipe + #1040, #' '
static pipe + #1041, #' '
static pipe + #1042, #' '
static pipe + #1043, #' '
static pipe + #1044, #' '
static pipe + #1045, #' '
static pipe + #1046, #' '
static pipe + #1047, #' '
static pipe + #1048, #' '
static pipe + #1049, #' '
static pipe + #1050, #' '
static pipe + #1051, #' '
static pipe + #1052, #' '
static pipe + #1053, #' '
static pipe + #1054, #' '
static pipe + #1055, #' '
static pipe + #1056, #' '
static pipe + #1057, #' '
static pipe + #1058, #' '
static pipe + #1059, #' '
static pipe + #1060, #'|'
static pipe + #1061, #'|'
static pipe + #1062, #' '
static pipe + #1063, #' '
static pipe + #1064, #' '
static pipe + #1065, #' '
static pipe + #1066, #' '
static pipe + #1067, #' '
static pipe + #1068, #' '
static pipe + #1069, #' '
static pipe + #1070, #' '
static pipe + #1071, #' '
static pipe + #1072, #' '
static pipe + #1073, #' '
static pipe + #1074, #' '
static pipe + #1075, #' '
static pipe + #1076, #' '
static pipe + #1077, #' '
static pipe + #1078, #' '
static pipe + #1079, #' '
static pipe + #1080, #' '
static pipe + #1081, #' '
static pipe + #1082, #' '
static pipe + #1083, #' '
static pipe + #1084, #' '
static pipe + #1085, #' '
static pipe + #1086, #' '
static pipe + #1087, #' '
static pipe + #1088, #' '
static pipe + #1089, #' '
static pipe + #1090, #' '
static pipe + #1091, #' '
static pipe + #1092, #' '
static pipe + #1093, #' '
static pipe + #1094, #' '
static pipe + #1095, #' '
static pipe + #1096, #' '
static pipe + #1097, #' '
static pipe + #1098, #' '
static pipe + #1099, #' '
static pipe + #1100, #'|'
static pipe + #1101, #'|'
static pipe + #1102, #' '
static pipe + #1103, #' '
static pipe + #1104, #' '
static pipe + #1105, #' '
static pipe + #1106, #' '
static pipe + #1107, #' '
static pipe + #1108, #' '
static pipe + #1109, #' '
static pipe + #1110, #' '
static pipe + #1111, #' '
static pipe + #1112, #' '
static pipe + #1113, #' '
static pipe + #1114, #' '
static pipe + #1115, #' '
static pipe + #1116, #' '
static pipe + #1117, #' '
static pipe + #1118, #' '
static pipe + #1119, #' '
static pipe + #1120, #' '
static pipe + #1121, #' '
static pipe + #1122, #' '
static pipe + #1123, #' '
static pipe + #1124, #' '
static pipe + #1125, #' '
static pipe + #1126, #' '
static pipe + #1127, #' '
static pipe + #1128, #' '
static pipe + #1129, #' '
static pipe + #1130, #' '
static pipe + #1131, #' '
static pipe + #1132, #' '
static pipe + #1133, #' '
static pipe + #1134, #' '
static pipe + #1135, #' '
static pipe + #1136, #' '
static pipe + #1137, #' '
static pipe + #1138, #' '
static pipe + #1139, #' '
static pipe + #1140, #'|'
static pipe + #1141, #'|'
static pipe + #1142, #' '
static pipe + #1143, #' '
static pipe + #1144, #' '
static pipe + #1145, #' '
static pipe + #1146, #' '
static pipe + #1147, #' '
static pipe + #1148, #' '
static pipe + #1149, #' '
static pipe + #1150, #' '
static pipe + #1151, #' '
static pipe + #1152, #' '
static pipe + #1153, #' '
static pipe + #1154, #' '
static pipe + #1155, #' '
static pipe + #1156, #' '
static pipe + #1157, #' '
static pipe + #1158, #' '
static pipe + #1159, #' '
static pipe + #1160, #' '
static pipe + #1161, #' '
static pipe + #1162, #' '
static pipe + #1163, #' '
static pipe + #1164, #' '
static pipe + #1165, #' '
static pipe + #1166, #' '
static pipe + #1167, #' '
static pipe + #1168, #' '
static pipe + #1169, #' '
static pipe + #1170, #' '
static pipe + #1171, #' '
static pipe + #1172, #' '
static pipe + #1173, #' '
static pipe + #1174, #' '
static pipe + #1175, #' '
static pipe + #1176, #' '
static pipe + #1177, #' '
static pipe + #1178, #' '
static pipe + #1179, #' '
static pipe + #1180, #'|'
static pipe + #1181, #'|'
static pipe + #1182, #' '
static pipe + #1183, #' '
static pipe + #1184, #' '
static pipe + #1185, #' '
static pipe + #1186, #' '
static pipe + #1187, #' '
static pipe + #1188, #' '
static pipe + #1189, #' '
static pipe + #1190, #' '
static pipe + #1191, #' '
static pipe + #1192, #' '
static pipe + #1193, #' '
static pipe + #1194, #' '
static pipe + #1195, #' '
static pipe + #1196, #' '
static pipe + #1197, #' '
static pipe + #1198, #' '
static pipe + #1199, #' '